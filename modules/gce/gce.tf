####### SAMPLE INSTANCE #########

resource "google_compute_instance" "gce_instance" {
   name          = var.host_name
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"
  
  
  
  boot_disk {
    initialize_params {
      image = "rhel-cloud/rhel-7"
          size = 20
    }
  }
 

  network_interface {
    subnetwork = "subnet1"

    
  }

  metadata = {
    team = "test"
  }

}
