provider "google" {
  
  credentials = var.cred
  project     = "${var.project_id}"
  region      = "${var.project_region}"
  version     = "~> 3.21"
}
