variable "project_id" {
  type        = "string"
  description = "The name of the project to instanciate the instance at."
  default     = "test"
}

variable "project_region" {
  type        = "string"
  description = "Region where you want to deploy the resources."
  default     = "us-central1"
 }


variable "cred" {
  type        = "string"
  description = "Region where you want to deploy the resources."
  default     = ""
 }


